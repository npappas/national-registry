FROM python:3.9-alpine

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN apk update && \
    apk add git curl wget zip

RUN mkdir /app
WORKDIR /app
COPY . /app/

RUN pip install --upgrade pip
RUN pip install -e .

