"""nationalRegistry URL Configuration"""
from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView

from .settings import DEBUG

# Default urls
urlpatterns = [
    path('admin/', admin.site.urls),
]

if DEBUG:
    urlpatterns += [
        path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    ]

    # OpenAPI 3
    urlpatterns += [
        # Schema download
        path('docs/schema/', SpectacularAPIView.as_view(), name="schema"),
        # Optional UI
        path('docs/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
        path('docs/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
    ]


# App based urls
urlpatterns += [
    path('api/', include('evidences.urls')),
    path('users/', include('users.urls')),
]
