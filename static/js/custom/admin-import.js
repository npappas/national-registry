let opts = {
  lines: 12, // The number of lines to draw
  length: 28, // The length of each line
  width: 18, // The line thickness
  radius: 42, // The radius of the inner circle
  scale: 1, // Scales overall size of the spinner
  corners: 1, // Corner roundness (0..1)
  speed: 1.2, // Rounds per second
  rotate: 0, // The rotation offset
  animation: 'spinner-line-fade-default', // The CSS animation name for the lines
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#ffffff', // CSS color or array of colors
  fadeColor: 'black', // CSS color or array of colors
  top: '50%', // Top position relative to parent
  left: '50%', // Left position relative to parent
  shadow: '1', // Box-shadow for the lines
  zIndex: 2000000000, // The z-index (defaults to 2e9)
  className: 'spinner', // The CSS class to assign to the spinner
  position: 'absolute', // Element positioning
};

$( document ).ready(function (){
    let download_evidences_btn = $('#download_evidences_btn');

    let target = document.getElementById('container');
    let spinner = new Spin.Spinner(opts);


    $(download_evidences_btn).click( (e) => {
        $(target).css('opacity', '0.8');
        spinner.spin(target);

        $.get('http://localhost:8000/api/get_evidences_from_emd/').done( (data) => {
            // console.log(JSON.parse(data));
            const bytes = new TextEncoder().encode(data);
            const blob = new Blob([bytes], {
                type: "application/json;charset=utf-8"
            });

            saveAs(blob, "evidences.json") ;
        }).fail( error => {
            console.log(error)
        }).always( ()=>{
            spinner.stop(target);
        })
    });
});

