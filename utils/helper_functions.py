import logging

logger = logging.getLogger(__name__)


def parse_processes(processes):
    """Parse dict processes and transform to array
    Args:
        processes: The processes fetched from National Procedures Registry
    Returns:
        emd_output_array: The processes as array
    Raises:

    """

    output_array = []

    for process in processes:
        curr_evidences = list(list(process.values())[0]['printouts'].values())[0]

        if len(curr_evidences) != 0:
            # Remove trailing dots
            # Remove spaces at the beginning and at the end of the string
            formatted_evidences = [s.strip().rstrip('.').strip('\"') for s in curr_evidences]
            output_array += formatted_evidences

    # sort
    output_array.sort()
    # remove duplicates
    emd_output_array = list(set(output_array))

    return emd_output_array





