"""
A module for storing configuration for accessing mediawiki site
Default values are provided, but passwords must be changed in docker-compose file
"""
import logging
import os

import environ
from nationalRegistry.settings import env

logger = logging.getLogger(__name__)


class MediaWikiConfig:
    _DEFAULT_SCHEME = 'https'
    _DEFAULT_PATH = '/'
    _DEFAULT_URL = 'reg-diavlos.gov.gr'
    env = environ.Env(
        EMD_SCHEME=(str, 'https')
    )

    def __init__(self, lang='gr'):
        self.lang = lang

        # getting configuration from .env
        self.scheme = env.str('EMD_SCHEME')
        self.path = env.str('EMD_PATH')

        if lang == 'gr' or lang == 'GR':
            self.url = env.str('EMD_EL_BASE_URL')
            self.username = env.str('EMD_EL_USERNAME')
            self.password = env.str('EMD_EL_PASSWORD')
        else:
            self.url = env.str('EMD_EN_BASE_URL')
            self.username = env.str('EMD_EN_USERNAME')
            self.password = os.getenv('EMD_EN_PASSWORD', 'CHANGEME')
            # self.password = env('EMD_EN_PASSWORD')

    def __str__(self):
        return self.lang
