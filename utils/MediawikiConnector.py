import mwclient
import tablib
import logging
from .MediaWikiConfig import MediaWikiConfig
from .helper_functions import parse_processes

logger = logging.getLogger(__name__)


class Site:
    _DEFAULT_LANG = 'gr'
    _DEFAULT_SCHEME = 'https'
    _DEFAULT_PATH = '/'

    def __init__(self, lang=_DEFAULT_LANG):
        self.lang = lang
        self.__client = None
        self.__config = None
        self._logged_in = False
        self.api = self._client.api
        self.get = self._client.get

    @property
    def _client(self):
        if self.__client is None:
            client = mwclient.Site(
                self._config.url,
                scheme=self._config.scheme,
                path=self._config.path
            )

            self.__client = client
        return self.__client

    @property
    def _config(self):
        if self.__config is None:
            self.__config = MediaWikiConfig(self.lang)
        return self.__config

    def login(self, username='', password='', auto=False, force=False):
        """Login by username and password."""
        if self._logged_in and not force:
            return
        if auto:
            username = self._config.username
            password = self._config.password.lstrip('l')
        try:
            self._client.clientlogin(username=username, password=password)
        except (mwclient.errors.LoginError,
                mwclient.errors.APIError) as e:
            logger.error(e)
        else:
            self._logged_in = True

    def _call_to_api(self):
        english = True if self.lang == 'en' or self.lang == 'EN' else False
        limit = 500
        offset = 0
        parameters = ''

        processes = []

        while 1:
            try:
                askargs_conditions = f'Category:Services Registry' if english else \
                    f'Category:Κατάλογος Διαδικασιών'
                parameters = parameters + f'|limit={limit}' + f'|offset={offset}'
                mw_response = self.get(
                    'askargs', format='json',
                    api_version=3,
                    conditions=askargs_conditions,
                    parameters=parameters,
                    printouts='Process evidence description'
                )
            except mwclient.errors.APIError:
                logger.error(mwclient.errors.APIError)
                exit(1)
            else:
                if 'query-continue-offset' in mw_response:
                    offset = mw_response['query-continue-offset']
                    processes.extend(mw_response['query']['results'])
                else:
                    break

        return processes

    def get_evidences_from_emd(self):
        self.login(auto=True)

        processes = self._call_to_api()
        emd_evidences_array = parse_processes(processes)

        exported_data = tablib.Dataset()
        exported_data.headers = ['id', 'title', 'mw_uuid', 'mw_id']

        index = 0
        for i in emd_evidences_array:
            exported_data.append([index, i, '', ''])
            index = index + 1

        return exported_data.json
