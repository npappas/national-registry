# National Registry
Test project for a registry of evidences etc. used in [National Services Registry](https://reg-diavlos.gov.gr)

## Structure

The repository's stack is a django web application.

- `users` : A django app that hosts users info
- `evidences` : The evidences app that holds all info about evidences
- `utils` : Utils to fetch json structured evidences from National Service Registry
- `static` : CSS and JS
- `locale` : Translations 

## Installation

In a folder of your choice run `sh install.sh`.
This will install python3 and make a virtual environment called `venv`. Then it will install all libraries used.

## Development 

After installing, run:
1. `python manage.py makemigrations && python manage.py migrate`
2. `python manage.py createsuperuser`: Enter credentials for a superuser.
3. `python manage.py runserver`
4. Enter `http://localhost:8000/admin` and login with the credentials you gave.


### Api endpoints
- api/evidences : GET, POST, PUT, DELETE
- users/ : Same

With debug mode on:
- docs/swagger-ui/ : Swagger ui
- docs/schema/redoc/ : Another ui
- docs/schema/ : Schema download