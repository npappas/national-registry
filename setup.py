from setuptools import setup, find_packages

setup(
    name='registry',
    version='0.1.0',
    description='A test project for Nation registry',
    url="https://gitlab.grnet.gr/npappas/national-registry",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.9",
    packages=find_packages(),
    scripts=['manage.py'],
    install_requires = [
        "Django",
        "django-environ",
        "django-import-export",
        "djangorestframework",
        "drf-spectacular",
        "requests",
        "tablib",
        "mwclient @ git+https://github.com/mwclient/mwclient#egg=mwclient"
    ],
)
