from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import authentication

from .serializers import UserSerializer
from .models import User


class UserViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    serializer_class = UserSerializer
    queryset = User.objects.all().order_by('id')
    permission_classes = [permissions.IsAuthenticated]
