from django.db import models
from django.contrib.auth.models import AbstractUser

from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    afm = models.CharField(max_length=200, verbose_name=_('AFM'), blank=True, null=True)
    from_gsis = models.BooleanField(default=False)

    is_staff = models.BooleanField(default=True, help_text=_('Set True if user is allowed to enter admin interface'))

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
