#!/usr/bin/env bash

# Installs
dpkg -s "python3-pip" &> /dev/null
python=$(which python3-pip)
echo "$python"

if [ -z "$python" ]; then
  sudo apt-get install -y python3-pip
fi


python3 -m venv myenv
source venv/bin/activate

PIP=$(which pip || which pip3)
${PIP} install -e .

