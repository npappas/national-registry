from django.contrib import admin
from import_export import resources
from .models import Evidence
from import_export.admin import ImportExportModelAdmin


class EvidenceResource(resources.ModelResource):
    class Meta:
        model = Evidence
        import_id_fields = ('title',)
        exclude = ('id', 'updated_date',)
        skip_unchanged = True
        fields = (
            'title',
        )


class EvidenceAdmin(ImportExportModelAdmin):
    resource_class = EvidenceResource
    ordering = ('title', )


admin.site.register(Evidence, EvidenceAdmin)
