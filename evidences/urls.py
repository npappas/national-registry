from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'evidences', views.EvidenceViewSet)

urlpatterns = [
    path('get_evidences_from_emd/', views.get_evidences_from_emd, name='get_evidences_from_emd'),

    path('', include(router.urls)),

]