from django.db import models
import uuid
from django.utils.translation import ugettext_lazy as _


class Evidence(models.Model):
    title = models.TextField(help_text=_('Evidence title'), unique=True)
    mw_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True, help_text=_('Evidence uuid'))
    mw_id = models.CharField(max_length=255, help_text=_('Evidence id'))

    class Meta:
        verbose_name = 'Δικαιολογητικό'
        verbose_name_plural = 'Δικαιολογητικά'

    def __str__(self):
        return self.title
