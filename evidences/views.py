from django.contrib import admin
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, schema
from rest_framework.permissions import IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly

# App specific imports
from .models import Evidence
from .serializers import EvidenceSerializer

from utils.MediawikiConnector import Site

admin.autodiscover()


class EvidenceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows evidences to be viewed or edited.
    """
    queryset = Evidence.objects.all().order_by('title')
    serializer_class = EvidenceSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]  # Only allow authenticated users to POST


@api_view(['GET'])
@schema(None)
def get_evidences_from_emd(request):
    lang = request.GET.get('lang', 'gr')  # set language for emd

    site = Site(lang=lang)

    evidences_json = site.get_evidences_from_emd()
    if evidences_json is None:
        return Response({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(evidences_json, status=status.HTTP_200_OK)
